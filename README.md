# expo-docker-quick-start

Generic docker project set up for expo react native project

# Set up
Create a new .env file from the .env.dist file
```
cp .env.dist .env
```

Update the react native packager hostname ip with your local ip address
```
REACT_NATIVE_PACKAGER_HOSTNAME=LOCAL_IP
```

If you already have a react native repo, clone the repo to new code folder
```
git clone YOUR_REACT_NATIVE_REPO code
```

Run docker-compose
```
docker-compose up
```