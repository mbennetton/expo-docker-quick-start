#!/bin/bash

init() {
    # If project doesn't exsit create a blank project
    if [ -z "$(ls -A .)" ]; then
        # Expo init doesn't allow specifying a folder so need to go up a directory first and then back to workdir
        cd .. && \
        expo init ${PROJECT_NAME} --template ${EXPO_TEMPLATE} && \
        cd ${PROJECT_NAME};
    fi

    npm install;
    expo start --clear;
}

init ${@}